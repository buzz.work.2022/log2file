use log::{LevelFilter};
use log4rs::{append::rolling_file::{policy::compound::{roll::fixed_window::FixedWindowRoller, trigger::size::SizeTrigger, CompoundPolicy}, RollingFileAppender}, Config, config::{Appender, Root}, filter::threshold::ThresholdFilter, encode::pattern::PatternEncoder};

pub fn init_log(level: &str, path: &str) {
    let level = get_level(level);

    let file_name = format!("{}{{}}", path);
    let fied_roller = FixedWindowRoller::builder().build(file_name.as_str(), 3).unwrap();

    let roller_file_limit = 5000 * 102400;
    let roller_trigger = SizeTrigger::new(roller_file_limit);
    let compound_policy = CompoundPolicy::new(Box::new(roller_trigger), Box::new(fied_roller));

    let config = Config::builder().appender(Appender::builder().filter(Box::new(ThresholdFilter::new(level))).build("logfile", Box::new(RollingFileAppender::builder().encoder(Box::new(PatternEncoder::new("{d(%+)(utc)} [{f}:{L}] {h({l})} {M}:{m}{n}"))).build(file_name, Box::new(compound_policy)).unwrap())));
    let c = config.build(Root::builder().appender("logfile").build(level)).unwrap();
    log4rs::init_config(c).unwrap();
}

fn get_level(level: &str) -> LevelFilter {
    let level = level.to_lowercase();
    match level.as_ref() {
        "debug" => LevelFilter::Debug,
        "trace" => LevelFilter::Trace,
        "warn" => LevelFilter::Warn,
        "info" => LevelFilter::Info,
        "error" => LevelFilter::Error,
        _ => LevelFilter::Off,
    }
}

#[cfg(test)]
mod tests {
    use log::{LevelFilter, debug, error, info};

    use crate::{get_level, init_log};

    #[test]
    fn check_get_level_lowercase() {
        assert_eq!(get_level("debug"), LevelFilter::Debug);
    }

    #[test]
    fn check_get_level() {
        assert_eq!(get_level("Debug"), LevelFilter::Debug);
    }

    #[test]
    fn check_level_off() {
        assert_eq!(get_level("asdasdadad"), LevelFilter::Off)
    }

    #[test]
    fn check_run() {
        init_log("debug", "test.log");
        debug!("Test debug");
        info!("Test Info");
        error!("test error");
    }
}
